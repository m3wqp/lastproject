import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import style from './HeaderRow.module.css'


const HeaderRow = () =>{
  return (
    <Container >
      <Row className={style.containerRow}>
        <Col className={style.containerCol} xs={{order: 'first'}}>

        </Col>
        <Col className={style.containerColCenter} xs>

        </Col>
        <Col className={style.containerCol} xs={{order: 'last'}}>

        </Col>
      </Row>
    </Container>
  )
}
export default HeaderRow;