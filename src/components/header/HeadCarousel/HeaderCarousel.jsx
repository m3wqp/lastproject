import React from "react";
import {Carousel, Col, Container, Row} from "react-bootstrap";
import port from '../../../img/index.png'
import headPort from '../../../img/head-port.jpg'
import styles from './Carousel.module.css'

const HeaderCarousel = () => {
  return (
    <>
      <Carousel>
        <Carousel.Item>
          <Carousel.Caption className={styles.carousel_container}>
            <span className={styles.carousel_span}>Мы предоставляем</span>
            <h2 className={styles.carousel_title}>Лучшие Логистический Услуги</h2>
            <span className={styles.carousel_span}>По  всей России</span>
          </Carousel.Caption>
          <img
            className=" carousel-icon d-block vh-100 w-100"
            src={port}
            alt='Port'
          />

        </Carousel.Item>

        <Carousel.Item className='carousel-container'>
          <Carousel.Caption className={styles.carousel_container}>
            <span className={styles.carousel_span}>Надежно. Качественно. В срок.</span>
            <h2 className={styles.carousel_title}>Грузовые перевозки по земле, воде и воздуху.</h2>
            <span className={styles.carousel_span}>Расстояние - это миф. И мы это доказываем.</span>
          </Carousel.Caption>
          <img
            className="carousel-icon d-block vh-100 w-100"
            src={headPort}
            alt='Port'
          />
        </Carousel.Item>

      </Carousel>
    </>
  )
}


export default HeaderCarousel;