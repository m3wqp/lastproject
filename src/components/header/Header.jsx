import React from 'react';
import style from './header.module.css'
import HeaderNav from "./HeadNav/HeaderNav";
import HeaderRow from "./HeaderRow/HeaderRow";



const Header = () => {
  return (
    <div className={style.header_Main_Container}>
      <HeaderNav/>
      <HeaderRow/>
    </div>
  )
}

export default Header;