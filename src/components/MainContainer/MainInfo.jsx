import React from "react";
import {NavLink} from "react-router-dom";
import feature from '../../img/feature.jpg'
import style from './MainInfo.module.css'

const MainInfo = () => {
  return(
    <div className={style.mainInfo_container}>
      <div>
        <h2>
          Давайте распространять <span className={style.mainInfo_span}>продукты</span>  по всему миру
        </h2>
        <p>
          Транспортная компания  — надежный грузоперевозчик с солидным опытом работы. Мы предлагаем отправку товаров из Набережных Челнов в любые города России между нашими филиалами.
        </p>
        <NavLink to={''}>

        </NavLink>
      </div>
      <div>
        <img className={style.infoIcon} src={feature} alt=""/>
      </div>

    </div>
  )
}

export default MainInfo;