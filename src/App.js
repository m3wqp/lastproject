import './App.css';
import Header from "./components/header/Header";
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter} from "react-router-dom";
import MainInfo from "./components/MainContainer/MainInfo";



function App() {
  return (

    <div className="App">
      <BrowserRouter>
        <Header/>
        <MainInfo/>
      </BrowserRouter>
    </div>

  )
}

export default App;
